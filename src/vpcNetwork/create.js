const create = () => {
  const vpc = {
    definition: {}
  }
  vpc.withBucketName = (name) => {
    vpc.definition.bucketName = name
    return vpc
  }
  vpc.withChangeSetName = (name) => {
    vpc.definition.changeSetName = name
    return vpc
  }
  vpc.withVpcName = (name) => {
    vpc.definition.vpcName = name
    return vpc
  }
  return vpc
}

module.exports = create
