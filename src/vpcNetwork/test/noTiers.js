/* globals describe it */

require('chai').should()

describe('create (with no tiers)', () => {
  it('adds changeSetName to the definition', () => {
    const create = require('../create')
    const vpc = create()
      .withChangeSetName('some name')
    vpc.definition.should.have.property('changeSetName').that.equals('some name')
  })
  it('adds bucketName to the definition', () => {
    const create = require('../create')
    const vpc = create()
      .withBucketName('some bucket')
    vpc.definition.should.have.property('bucketName').that.equals('some bucket')
  })
  it('adds vpcName to the definition', () => {
    const create = require('../create')
    const vpc = create()
      .withVpcName('some vpc name')
    vpc.definition.should.have.property('vpcName').that.equals('some vpc name')
  })
  it('allows chaining of calls', () => {
    const create = require('../create')
    const vpc = create()
      .withChangeSetName('some changeSet name')
      .withBucketName('some bucket name')
      .withVpcName('some vpc name')
    vpc.definition.should.have.property('changeSetName').that.equals('some changeSet name')
    vpc.definition.should.have.property('bucketName').that.equals('some bucket name')
    vpc.definition.should.have.property('vpcName').that.equals('some vpc name')
  })
})
