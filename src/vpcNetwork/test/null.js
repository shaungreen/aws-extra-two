/* globals describe it */

require('chai').should()

describe('create (null case)', () => {
  describe('when vpcNetwork is defined with no further information', () => {
    it('creates an expected definition object', () => {
      const create = require('../create')
      const vpc = create()
      vpc.should.have.property('definition')
      vpc.definition.should.be.an('object')
    })
  })
})
